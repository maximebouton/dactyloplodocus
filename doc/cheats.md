# Keys

F1         : Display caret infos and keys events in ready to modify lines,
F2         : Activate or desactivate periodic event triggering, speed define by this.speed,
F3         : Stop floodfill processing,
Enter      : Eval caret event define in this.e,
%          : Eval caret line position text content,
§          : Save in this.e caret line position text content,
#          : Place a new child caret in caret position,
$          : Delete all child caret,
Backspace  : Moves the cursor backwards, deletes the character,
ArrowRight : Moves the cursor forwards,
ArrowLeft  : Moves the cursor backwards,
ArrowUp    : Moves the cursor to the next line upwards,
ArrowDown  : Moves the cursor to the next line below,
End        : Moves the cursor to the end of the line,
Home       : Moves the cursor to the first position of the line,
PageUp     : Moves the cursor to the first line,
PageDown   : Moves the cursor to the last line,
Delete     : Erase the cursor line,        
Tab        : Clear the screen,
F12        : Rebuild Dactyloplodocus

# Floodfill

## options

hard            : true or false
data            : "string", ["class"]
speed           : int
dirs            : [pt,pt,pt,…]
lifetime        : int
trigger         : true or false
temp            : true or false

# displaying a text file :

    this.display("file.txt",x)

## text files :

    boy.txt
    caroline.txt
    death.txt
    girl.txt
    google.txt
    ned.txt
    neko.txt
    norm.txt
    tama.txt
    title.txt
    tom.txt
    walking.txt

# sentences
s = {}

## pain
s.p = ["!Ouch!","!Aïe!","!Ouille!","!Sssh…","!Arg…"]

# animations

w = {} // it was for weapons in the game branch

## how to animate a floodfill

                             place r.a() result in a array
                             ↓    ↓
    this.floodfill({data:r.a([w[…]]),dirs:[pt,pt,pt,…],temp:true})
                              ↑                        ↑
                              any array will work      this will erase data over time
                                                       result animate display

## bone 
w.b = "-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/"

## leaf
w.l = "☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘❧❦☙☘…

# figures
f = {}

## player
f.p = ["(•ᴗ•)","(-ᴗ-)"]

## monster
f.m = ["༼◐ᗣ◐༽","༼◑ᗣ◑༽"]

# characters (l is for letters)
l = {}

## blocks
l.b = [' ','░','▒','▓','█']

## tree
l.t = ['│','┤','╡','╢','╖','╕','╣','║','╗','╝','╜','╛','┐','└','┴','┬','├','─','┼','╞','╟','╚','╔','╩','╦','╠','═',…

## Dingbats /!\
l.d = ['✀','✁','✂','✃','✄','✅','✆','✇','✈','✉','✊','✋','✌','✍','✎','✏','✐','✑','✒','✓','✔','✕','✖','✗','✘','✙',…

## smiley /!\
l.s = ['☺', '☻','ツ']

## squares /!\
l.q = ['▢','▣','▤','▥','▦','▧','▨','▩']

## genres /!\
l.g = ['⚢','⚣','⚤','⚥','⚦','⚧','⚨','⚩']

## ascii
l.a = ['☺','☻','♥','♦','♣','♠','•','◘','○','◙','♂','♀','♪','♫','☼','►','◄','↕','‼','¶','§','▬','↨','↑','↓','→','←',…

## arabic /!\
l.r = ['ؠ','ء','آ','أ','ؤ','إ','ئ','ا','ب','ة','ت','ث','ج','ح','خ','ذ','ر','آ','ز','س','ش','ص','ض','ط','ظ','ع','غ',…

# backgrounds
b = {}

## reds
b.r = ["red","darkred","mediumvioletred","indianred","purple","rebeccapurple","mediumpurple","blueviolet",…

## blues
b.b = ["blue","darkblue","mediumblue","deepskyblue","midnightblue","dodgerblue","royalblue","steelblue",…

## css
b.c = ["black","silver","gray","grey","white","maroon","red","purple","fuchsia","green","lime","olive",…

## grey
b.g = [
    'grey0',
    'grey1',
    'grey2',
    'grey3',
    'grey4',
    'grey5',
    'grey6',
    'grey7',
    'grey8',
    'grey9',
    'grey10',
    'grey11',
    'grey12',
    'grey13',
    'grey14',
    'grey15'
]

## outline
b.o = [
    'dotted',
    'dashed',
    'solid',
    'double',
    'groove',
    'ridge',
    'inset',
    'outset',
    'none'
]

## pattern /!\
b.p = ['4pointstars','anchorsaway','architect','autumn','aztec','bamboo','banknote','bathroomfloor',…

# dimension
d = {} // return a dimension object, sorry but dont remember use example

## coor
d.c(coor)

## random
d.r() 

## centerX
d.cX()

## centerY
d.cY()

## centerX
d.X()

## centerY
d.Y()

# counters

ct = {}

## array

ct.a = function(arr,jump,caret) // dont remember the use

# rdm
r = {}

## int
r.i(min, max)

## float
r.f(min, max)

## array
r.a(arr)

## object
r.o(obj)

## dimension
r.d()

## point
r.p(n, min, max)

## class
r.c()

## characters
r.l(n,chars)

# Game of Life
    lifedefaults = {
        'cycles': 1,
        'sleep': 1000,
        'age': 0,
        'dead': '',
        'neighbours': [
                        [-1,-1],[ 0,-1],[ 1,-1],
                        [-1, 0],        [ 1, 0],
                        [-1, 1],[ 0, 1],[ 1, 1]
                      ]
    }

