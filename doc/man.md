## Dimension

`Dimension(type,context)` est un objet pouvant contenir d'autres objets
`Dimension` (`Dimension.content`) et/ou être contenu dans un autre objet
`Dimension` (`Dimension.context`). Il peut être arbitrairement caractérisé par
un `type`.

Exemple : new Dimension("ligne") un objet destiné à contenir des
`Dimension("caracteres")` et à être contenu dans une `Dimension("grille")`.

## Floodfill

### Animation (anim)

    anim : true|false, replace : [array]|"string"

Permet de faire évoluer la valeur de remplissage du floodfill via une suite de
données de remplacement. À chaque occurence du floodfill un index augmente de 1
définissant la valeur choisie au sein de la suite. Lorsque l'index est
supérieur à la longueur de la suite il retourne à 0, ce qui produit un cycle
"animé".

    // exemple

    matrix = new Dimension({"matrix"})
    matrix.dig({"line":5,"type":26})
    matrix.dive(2,2).floodfill({ anim:true, replace:"ABCD", dirs:[[0,1],[1,0]] })

    /* occurences
    
    0 : index = 0, replace = 'A'
    
    +--------------------------+
    |                          |
    |                          |
    |  A                       |
    |                          |
    |                          |
    +--------------------------+

    1 : dir = [0,1], index = 1, replace = 'B'
    
    +--------------------------+
    |                          |
    |                          |
    |  AB                      |
    |                          |
    |                          |
    +--------------------------+

    2 : dir = [1,0], index = 2, replace = 'C'
    
    +--------------------------+
    |                          |
    |                          |
    |  AB                      |
    |  C                       |
    |                          |
    +--------------------------+

    3 : dir = [0,1], index = 3 : replace = 'D'
    
    +--------------------------+
    |                          |
    |                          |
    |  ABD                     |
    |  C                       |
    |                          |
    +--------------------------+

    4 : dir = [1,0], index = 0, replace = 'A'
    
    +--------------------------+
    |                          |
    |                          |
    |  ABD                     |
    |  CA                      |
    |                          |
    +--------------------------+

    5 : dir = [0,1], false
    
    +--------------------------+
    |                          |
    |                          |
    |  ABD                     |
    |  CA                      |
    |                          |
    +--------------------------+

    6 : dir = [1,0], index = 0, replace = 'B'
    
    +--------------------------+
    |                          |
    |                          |
    |  ABD                     |
    |  CA                      |
    |  B                       |
    +--------------------------+

    ...

### Directions (dirs)

    dirs : [ ... [c], [b], [a] ]

Définit les 
