// # Dimension
// 
// dimension is an object design to build multi-dimentionnal matrix and caracterize each dimension with a type 
// dimension can stock data

Dimension = function(type,context){
    this.type = type
    this.context = context
    this.content = []
    this.data = {
        soft:'',
        hard:''
    }
    this.memory = 0
    this.floodfills = {}
    return this
}

// ### root

Dimension.prototype.root = function(){
    return this.context ? this.context.root() : this
}

// ### dig
//
// build in-depth dimensions, e.g `dim.dig({line:8,type:8})` would create 8 dimensions of type _line_ 
// inside dim, and fill each of theses _lines_ with 8 dimensions of type _type_ (typographic)

Dimension.prototype.dig = function(content){
    depth = Object.keys(content)[0]
    for ( var i = 0; i < content[depth]; i++ ){ 
        this.content.push(new Dimension(depth,this))    
    }
    delete content[depth]
    if ( !Object.keys(content).length ) return this.root().content
    for( var i = 0; i < this.content.length; i++ ) {
        if(i == this.content.length-1){
            return this.content[i].dig(JSON.parse(JSON.stringify(content)))
        }else{
            this.content[i].dig(JSON.parse(JSON.stringify(content)))
        } 
    }
}

// ## call

Dimension.prototype.call = function(here){
    if ( !this.content.length ) return false
    if(!here) here = []
    for( var i = 0; i < this.content.length; i++ ) {
        here.push(this.content[i])
        if(i == this.content.length-1){
            if(this.content[i].content.length) {
                return this.content[i].call(here)
            }else{
                return here
            }
        }else{ 
            if(this.content[i].content.length) this.content[i].call(here)
        } 
    }
}

// ## view

Dimension.prototype.view = function(range,view,level){
    if ( !this.content.length ) return false
    if(!view) view = []
    level = level == undefined ? 0 : level + 1
    for( var i = range[level][0]; i < range[level][0] + range[level][1] ; i++ ) {
        if(!this.content[i]) continue
        view.push(this.content[i])
        if( i == this.content.length-1 || i == range[level][0] + range[level][1] - 1 ) { 
            if(this.content[i].content.length) {
                return this.content[i].view(range,view,level)
                break
            }else{
                return view
                break
            }
        }else{ 
            if (this.content[i].content.length) this.content[i].view(range,view,level)
        } 
    }
}

// ## fill

Dimension.prototype.fill = function(type,data){
    this.call().forEach( dimension => {
        if(dimension.type == type){
            dimension.data.hard = data
        }
    })
}

// ## print

Dimension.prototype.print = function(range){
    var string = ""
    this.view(range).forEach( dimension => {
        string += dimension.data.soft.length ? dimension.data.soft : dimension.data.hard
        if(dimension.memory){
            dimension.memory--
        }else{
            dimension.data.soft = ''
        }
    })
    return string
}

// ### dive
// 
// return contained dimensions from index, e.g `dim.dig([2,4])` would return the fourth contained dimension 
// inside the second dimension of dim content (ctn)

Dimension.prototype.dive = function(id) { // (index)
    if ( !id.length ) return this
    return this.content[id[0]] ? this.content[id[0]].dive(id.splice(1,id.length-1)) : false
}

// ### get
//

Dimension.prototype.get = function(dir){
    depth = dir.length
    origin = this
    for ( var i = 0; i < depth; i++ ){ 
        origin = origin.context 
    }
    position = []
    id = this.id()
    offset = id.length-depth
    for ( var i = offset; i < id.length; i ++ ){
        position.push(dir[i-offset]+id[i])
    }
    return origin.dive(position)
}

// ### id (index)
// 
// return index array of a dimension, _cad_ position of it and recursively it contexts (ctx) from _main_ 
// (dimension containing all the other) to down (the dimension aims by id() function itself)

Dimension.prototype.id = function(){
    var ref = this
    var id = []
    while ( ref.context ) {
        id.push(ref.context.content.indexOf(ref))
        ref = ref.context
    }
    return id.reverse()
}

// ## remove

Dimension.prototype.remove = function(){
    this.data.hard = ' '
    this.context.content.splice(this.context.content.indexOf(this),1)
    this.context.content.push(this)
    return this.content
}

// ## insert

Dimension.prototype.insert = function(data){
    var dimension = new Dimension(this.type,this.context)
    dimension.data.hard = data
    this.context.content.splice(this.context.content.indexOf(this),0,dimension)
    this.context.content.pop()
}
