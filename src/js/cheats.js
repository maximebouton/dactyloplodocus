// # dirs

d = {}

// ## diamond

d.d = [
                [-1, 0],
        [ 0,-1],        [ 0, 1],
                [ 1, 0]
]

// # anims

a = {}

// ## shades

a.s = " ░▒▓▒░"

// ## draw


a.d = "_.-~-."

// ## grow

a.g= ".oOo"

// ## turn

a.t= "-\\|//"

// # rdm
r = {};

// ## int
r.i = function(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}

// ## float
r.f = function(min, max) {
    return Math.random() * (max - min) + min;
}

// ## array
r.a = function(arr) {
    var n = r.i(0,arr.length);
    return arr[n];
}

// ## point
r.p = function(n, min, max) {
	var pts=[]
    for(i=0;i<n;i++)pts.push([r.i(min,max),r.i(min,max)])
    return pts
}
