// ### floodfill

var floodfill_count = 0

Dimension.prototype.floodfill = function(options){
    if(!options) options = {}
    if(!options.floodfill){
        for(var i in s.f) if(options[i] == undefined) options[i] = s.f[i]()
        options.floodfill = floodfill_count
        options.frame = 0
        options.life = 0
        floodfill_count++
    }
    if(this.floodfills[options.floodfill]) return
    this.floodfills[options.floodfill] = true
    if(!options.poly){
        if(this.data[options.memory] == options.replace && !options.hard ) return
        root = this.root()
        if(!options.data || options.hard) {
            options.data = []
            for(var i = 0; i < root.content.length; i++){
                id = this.id()
                id.shift()
                options.data.push(root.content[i].dive(id).data[options.memory])
            }
        }
        for(var i = 0; i < root.content.length; i++){
            id = this.id()
            id.shift()
            ghost = root.content[i].dive(id).data[options.memory]
            if ( ghost != options.data[i] && !options.hard ) return
        }
    }else{
        if(!options.data || options.hard) options.data = this.data[options.memory]
        if(this.data[options.memory] == options.replace && !options.hard 
        || this.data[options.memory] != options.data && !options.hard ) return
    }
    if(options.life >= options.lifetime) return
    options.life++
    if(options.anim){
        this.data[options.memory] = options.replace[options.frame]
        options.frame = options.frame >= options.replace.length-1 ? 0 : options.frame + 1
    }else{
        this.data[options.memory] = options.replace
    }
    if(options.memory == "soft") this.memory = options.size
    if ( options.instant ){
        options.dirs.forEach( dir => {
                if(this.get(dir)) this.get(dir).floodfill(options)
        })
    }else{
        setTimeout(function(){
            options.dirs.forEach( dir => {
                    if(this.get(dir)) this.get(dir).floodfill(options)
            })
        }.bind(this),options.speed)
    }
}
