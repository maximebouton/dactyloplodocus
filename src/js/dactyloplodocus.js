// # Cursor

Cursor = function(dimension){
    this.position = dimension
    this.signal()
    return this
}

Cursor.prototype.signal = function(){
    this.position.memory = parseInt(1000 / s.r)
    data = this.position.data.soft
    this.position.data.soft = data != '█' ? '█' : ''
    setTimeout(function(){this.signal()}.bind(this),1000)
}

Cursor.prototype.move = function(dir){
    if(!this.position.get(dir)) return false
    this.position.memory = 0
    this.position = this.position.get(dir)
    this.position.memory = parseInt(1000 / s.r)
    this.position.data.soft = '█'
}

Cursor.prototype.replace = function(data){
    this.position.data.hard = data
    this.move([1])
}

Cursor.prototype.insert = function(data){
    this.position.insert(data)
}

Cursor.prototype.erase = function(){
    this.move([-1])
    this.position.data.hard = ''
}

Cursor.prototype.remove = function(){
    this.position.get([-1]).remove()
}

Cursor.prototype.clear = function(){
    this.position.floodfill({instant:true,hard:true,replace:' ',dirs:[[-1,0,0],[1],[-1]]})
}
