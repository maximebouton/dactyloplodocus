// # Settings

var s = {}

// ## drawSpeed

s.r = 20 // 1 draw call per 20 frames

// ## floodfill

s.f = {
    "anim" : function(){ return false },
    "dirs" : function(){ return [
            [-1,-1],[-1, 0],[-1, 1],
            [ 0,-1],        [ 0, 1],
            [ 1,-1],[ 1, 0],[ 1, 1],
    ]},
    "hard" : function(){ return false},
    "instant" : function(){ return false },
    "lifetime" : function(){ return Infinity },
    "memory" : function(){ return "hard" },
    "poly" : function(){ return false },
    "replace" : function(){ return r.a(" ░▒▓█-|\\//()[]_.-~'`<>!:;*#°,") },
    "size" : function(){ return 10 },
    "speed" : function(){ return 50 },
}
