/*******************************************************************************
 *                                                                             *
 *  Dactyloploplodocus is a multi-users live coding environment for creating   *
 *  text-based motion graphics.                                                *
 *                                                                             *
 *  Copyright © 2018 Jean-Morgane.                                             *
 *                                                                             *
 *  This program is free software: you can redistribute it and/or modify it    *
 *  under the terms of the GNU General Public License as published by the      *
 *  Free Software Foundation, either version 3 of the License, or (at your     *
 *  option) any later version. This program is distributed in the hope that    *
 *  it will be useful, but WITHOUT ANY WARRANTY; without even the implied      *
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the   *
 *  GNU General Public License for more details.You should have received a     *
 *  copy of the GNU General Public License along with this program. If not,    *
 *  see https://www.gnu.org/licenses                                           *
 *                                                                             *
 *  jean-morgane@keemail.com                                                   *
 *                                                                             *
 *  Press any key to continue.                                                 *
 *                                                                             *
 *******************************************************************************/

// # Caret

// ## global functions 

function selected_caret(){
    return carets[carets.length-1]
}

// ## prototype

Caret = function(options,owner){
    var setting=JSON.parse(JSON.stringify(settings.caret))
    for(var i in setting)this[i]=setting[i]
    for(var i in options)this[i]=options[i]
    /* temp? */ this.sound=new Sound(this);
    this.exist=true
    this.position=carets.length
    carets.push(this)
    this.move();this.signal()
}

Caret.prototype.signal = function(rec){
    if(!this.exist)return
    if(rec==undefined)rec=true
    if(!this.dimension.caret)linkCaret(this.dimension,this)
    if(this.dimension.dom.textContent==this.recto){
        if(this.dimension.data.length==1 && this.dimension.data!=this.recto){
            if(!this.verso){
            this.dimension.dom.textContent=this.dimension.data
            }else{
                this.dimension.dom.textContent=this.verso
            }
        }else{
            if(!this.verso){
                this.dimension.dom.textContent=' '
            }else{
                this.dimension.dom.textContent=this.verso
            }
        }
    }else{
        this.dimension.dom.textContent=this.recto
    }
    if(settings.multiplayer && id!=undefined)socket.emit("signal",[this.dimension.position,this.dimension.context.position],this.dimension.dom.textContent,id)
    if(rec)if(this.loop && !stop){try{eval(this.e)}catch(e){this.echo(e.message)}}
    if(rec)setTimeout(function(){this.signal()}.bind(this),this.speed)
}

function remove(hard){
    if(hard==undefined)hard=false
    dimension=selected_caret().dimension
    for(i=0;i<carets.length;i++){
        carets[i].dimension.caret=false;
        carets[i].exist=false
        if(carets[i].dimension.data.length==1){carets[i].dimension.dom.textContent=carets[i].dimension.data}else{carets[i].dimension.dom.textContent=' '}
        updateDom(carets[i].dimension)
    }; carets=[]; if(!hard){new Caret({dimension:dimension})}
}
Caret.prototype.remove = function(){
    linkCaret(this.dimension,false);
    this.exist=false
    if(this.dimension.data.length==1){this.dimension.dom.textContent=this.dimension.data}else{this.dimension.dom.textContent=' '}
    carets[this.position]=null
    updateDom(this.dimension)
}

Caret.prototype.move = function(dir){
    if(this.dimension.findRelative(dir)){linkCaret(this.dimension,false);this.dimension=this.dimension.findRelative(dir);linkCaret(this.dimension,this)}
}

Caret.prototype.erase = function(left){  
    if(left==undefined)left=true
    dataToReport=[]
    for(var i = this.dimension.position; i < this.dimension.context.content.length; i++){
        dataToReport.push(this.dimension.context.content[i].data)
    }
    if(left){
        for(var i=0; i<dataToReport.length; i++){
            if(this.dimension.context.content[i+this.dimension.position-1]){this.dimension.context.content[i+this.dimension.position-1].data=dataToReport[i];updateDom(this.dimension.context.content[i+this.dimension.position-1])}
        }
        this.move([this.dir[0]*-1,this.dir[1]*-1])
    }else{
        for(var i=dataToReport.length; i>0; i--){
            if(this.dimension.context.content[i+this.dimension.position+1]){this.dimension.context.content[i+this.dimension.position+1].data=dataToReport[i];updateDom(this.dimension.context.content[i+this.dimension.position+1])}
        }
        this.move([this.dir[0],this.dir[1]])
    }
}

Caret.prototype.trigger = function(string){
    try{eval(string)}catch(e){this.echo(e.message)}
}

Caret.prototype.drop = function(dimension){
    if(!dimension)dimension=this.dimension
    new Caret({dimension:dimension})
    selected_caret().move(selected_caret().dir)
}

Caret.prototype.display = function(textfile,mX){
    if(mX==undefined)mX=0
    read('src/text/'+textfile).then(function(content) {
        
        for(y=0;y<settings.textfiles[textfile][1];y++){
        events["onset"](this);this.move([mX,0])
        line=""
            for(x=0;x<settings.textfiles[textfile][0];x++){
                line+=content[x+y*settings.textfiles[textfile][0]]
            }
        this.echo(line,false,false)
        }
    }.bind(this));
}

Caret.prototype.echo = function(string,home,inline,soft){
	if(stop){stop=false};if(home==undefined)home=false;if(inline==undefined)inline=true;if(soft==undefined)soft=false
    this.floodfill({data:string,dirs:[[1,0]],speed:1,hard:true,soft:soft})
    this.move([string.length,0]);if(!inline)this.move([0,1]);if(home)events["onset"](this)
}

Caret.prototype.help = function(){
	events["top"](this);
	events["onset"](this);
	for(i in man.keys){
		this.echo("man.keys.",false,true);this.echo(i+" = ",false,true);this.echo('"'+man.keys[i]+'"',false,true);this.echo(' //'+man.events[man.keys[i]],true,false)
	}
    this.echo("this.e",false,true);this.echo(' = "'+this.e+'"',true,false)
    this.echo("this.speed",false,true);this.echo(' = "'+this.speed+'"',true,false)
    this.echo("this.loop",false,true);this.echo(' = "'+this.loop+'"',true,false)
	for(i in this.f){this.echo("this.f."+i,false,true);this.echo(' = "'+this.f[i]+'"',true,false)}
}

Caret.prototype.event = function(e) {

    if(settings.multiplayer && id!=undefined)socket.emit("event",e,id)

    if(this.dimension.data.length==1){
        if(!this.verso){
            this.dimension.dom.textContent=this.dimension.data
        }else{
            this.dimension.dom.textContent=this.verso
        }
    }
    if(this.dimension.dom)updateDom(this.dimension)
    if(events[man.keys[e.key]]!=undefined && !isCommand(e.key) || e.key=="F12"){
        events[man.keys[e.key]](this,e);if(this.dimension.dom)updateDom(this.dimension)
        this.dimension.dom.textContent=this.recto;return
    }else{if(isCommand(e.key))createEvent(e,this.dimension.context.dom.textContent)}
    if(events[e.key]!=undefined){
        events[e.key](this,e);if(this.dimension.dom)updateDom(this.dimension)
        this.dimension.dom.textContent=this.recto;return}
    if(e.key.length!=1)return
    dataToReport=[]
    for(var i = this.dimension.position; i < this.dimension.context.content.length; i++){
    	dataToReport.push(this.dimension.context.content[i].data)
    }
    for(var i=0; i<dataToReport.length; i++){
    	if(this.dimension.context.content[i+this.dimension.position+1]){this.dimension.context.content[i+this.dimension.position+1].data=dataToReport[i];updateDom(this.dimension.context.content[i+this.dimension.position+1])}
    }
    this.dimension.data=' '; this.dimension.data=e.key
    if(this.dimension.dom)updateDom(this.dimension)
    this.move(this.dir)
    if(this.dimension.dom)updateDom(this.dimension)
    this.dimension.dom.textContent=this.recto
}

Caret.prototype.floodfill= function(options){
    if(!options)options={}
    for(var i in floodfill)if(options[i]==undefined)options[i]=floodfill[i](options,this)
    for(var i in endings)if(endings[i](options,this))return
    options.dimension.floods[options.flood]=true; options.age++
    if(options.dimension.dom || options.dimension.dom)updateDom(options.dimension)
    setTimeout(function(){
        for(var i in options.dirs){
            var local={}
            for(var j in options)local[j]=options[j]
            if(options.dimension.findRelative(options.dirs[i])){
                local.dimension=options.dimension.findRelative(options.dirs[i])
                this.floodfill(local)
            }
        }
    }.bind(this),options.speed)
    return
}

Caret.prototype.life = function(options) {
    if (stop) return;
    if (!options) options = {};
    for (var i in lifedefaults) if (options[i] == undefined) options[i] = lifedefaults[i];
    if (!options.target) options.target = this;
    if (!options.data) options.data = options.target.dimension.data;

    if (options.age >= options.cycles) return;
    
    var dead = [];
    var live = [];

    setTimeout(function() {
        for (var y in options.target.dimension.context.context.content) {
            for (var x in options.target.dimension.context.context.content[y].content) {
                var n = options.target.dimension.context.context.content[y].content[x].neighbours(options.neighbours, options.data);
                if (options.target.dimension.context.context.content[y].content[x].data == options.data) {
                    if (n < 2 || n > 3) dead.push(options.target.dimension.context.context.content[y].content[x]);
                }
                if (options.target.dimension.context.context.content[y].content[x].data !== options.data) {
                    if (n == 3) live.push(options.target.dimension.context.context.content[y].content[x]);
                }
            }
        }
        for (var i in live) {
            live[i].data = options.data;
            updateDom(live[i]);
        }
        for (var i in dead) {
            dead[i].data = options.dead;
            updateDom(dead[i]);
        }
        options.target.life(options);

    }.bind(options.target), options.sleep)
    options.age ++;
}

